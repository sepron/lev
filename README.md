# lev

## Name
lev is an abbreviation of List Environment Variables matching names or values

## Description
show contents of environment variables and colorize matching excerpts of provided script arguments

## Usage
example
lev MATH MAN PATH MAP PAM; lev -v lua cmake emacs

## License
GPL3
